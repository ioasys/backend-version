- Como rodar a aplicacao em um servidor remoto para acesso?
Heroku faz deploy de aplicacoes, ent eu irei subir nossa aplicacao assim que finalizar as rotas de cadastro certinho.

- Como integrar as aplicacoes, front, back e app mobile?
Utilizar requisicoes em ambas as aplicacoes. O axios faz essas requisicoes p gnt, preciso estudar sobre e retornar aqui.
Adiantando, trabalharemos exclusivamente com JSONs, incluindo os formularios.

- Como realizar o "login" de usuarios de fato?
Utilizaremos autenticacao com hash JWT, preciso estudar melhor sobre isso tbm.

** Termos importantes: **
Axios
Bearer Token
JWT
Blood Bearer
Deploy


** Quem da sua equipe você indicaria como destaque? E por que? **

"Fernanda - META EXPLOIT. Na vdd, todos os membros deveriam ser destaque, e nem e "puxando sardinha" para meu time, e que realmente nos introsamos mt bem, cada um esta se ajudando, nossas reunioes nao sao pacatas cheias de termos formais como se estivessemos em uma apresentacao de TCC; nao que seja totalmente informal, ainda e trabalho, ainda precisa ser profissional. Mas eu acho que, a Fernanda, n tendo experiencia cm o trabalho dos devs, e se esforcando p entender cmo nos trabalhamos, cmo faremos tal tarefa, qual o tempo que devera ser gasto em tal task; juntamente a resiliencia de sempre perguntar se esta sendo "mandona" dms ou chata, se mostrou um membro em destaque p mim. Algumas ideias e sugestoes, principalmente no "soft project" (acho que este termo nem msm existe, mas quero dizer cm relacao a metodologia, ideias, definicoes de projeto) foram essenciais para chegar a ideia principal de nossa aplicacao."


** O que na sua opinião poderíamos fazer para melhorar sua experiência?  **

"Bom, apesar de ter curtido muito o camp e td minha experiencia ate entao, eu me deparei cm algumas dificuldades por n ter experiencia na area em si. Requisicoes, frameworks de rotas/requisicoes como Axios por exemplo (que inclusive ontem cv com Diego, backend, sobre cmo integrar front e mobile com o back em que sou responsavel, me foi sugerido utilizar este Axios) sao termos bem complexos p mim. Sim, e preciso estudar e claro, colocar a mao na massa p fixar e levar o aprendizado a um patamar digno do que esperam, mas acredito que uma forma de melhorar as experiencias dos campers nas proximas edicoes, seja trabalhar com varios exemplos praticos, pequenas aplicacoes msm, antes do desafio da parte 1, que inclusive tive mt dificuldade. Minha equipe utiliza de cadastro e login de usuarios, e a parte do cadastro, apos conseguir configurar o Sequelize e fazer a integracao com o banco remoto (Heroku), nao foi tao complicada, acho que daria p fazer uma aplicacao de cadastro dentro do camp, p familiarizar os campers cm essas tecnologias essenciais. Digo isso pq acredito que "cadastro" de usuarios ou qualquer outra coisa, seja uma funcionalidade bem "core" para qualquer dev."